1. PhilosopherWithFork, volatile, synchronized, deadLock.
2. PhilosopherWithNumericFork and no deadlock.
2.1. PhilosopherWithForkWithBusyFlag (volatile and atomic) and no deadlock.
3. PhilosopherWithUsualFork2 and ReentrantLock

RunPhilosopherWithIdFork
Philosopher [0] ate 373 times and waited 23043 ms
Philosopher [1] ate 373 times and waited 23103 ms
Philosopher [2] ate 409 times and waited 19762 ms
Philosopher [3] ate 407 times and waited 19830 ms
Philosopher [4] ate 450 times and waited 15936 ms

RunPhilosopherWithForkWithBusyFlag
Philosopher [0] ate 408 times and waited 19350 ms
Philosopher [1] ate 424 times and waited 17884 ms
Philosopher [2] ate 415 times and waited 18801 ms
Philosopher [3] ate 416 times and waited 18541 ms
Philosopher [4] ate 406 times and waited 19581 ms