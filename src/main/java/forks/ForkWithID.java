package forks;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Fork with unique id
 */
public final class ForkWithID implements Fork {

    private static AtomicInteger globalForkId = new AtomicInteger();

    public final int id;

    public ForkWithID(){
        System.out.println("globalForkId:" + globalForkId);
        id = globalForkId.getAndIncrement();
    }
}
