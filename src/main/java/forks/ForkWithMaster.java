package forks;

import philosophers.Philosopher;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * if some philosopher took this fork he mast
 */
public final class ForkWithMaster implements Fork {

    private final static int defaultMasterId = -1;

    /**
     * master id
     */
    private final AtomicInteger master = new AtomicInteger(defaultMasterId);

    public boolean isBusy() {
        return master.get() != defaultMasterId;
    }

    /**
     * took this fork
     * @return true if success, false if it already busy or bad lack
     */
    public boolean setBusy(Philosopher newMaster){
        return master.compareAndSet(defaultMasterId, newMaster.position);
    }

    /**
     * free this fork
     * @return true if success
     */
    public boolean setUnBusy(Philosopher oldMasterId) {
        return master.compareAndSet(oldMasterId.position, defaultMasterId);
    }
}
