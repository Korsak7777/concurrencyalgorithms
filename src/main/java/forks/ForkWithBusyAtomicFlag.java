package forks;

import java.util.concurrent.atomic.AtomicBoolean;

public final class ForkWithBusyAtomicFlag implements ForkWithBusyFlag{
    private final AtomicBoolean busy = new AtomicBoolean();

    @Override
    public boolean isBusy() {
        return busy.get();
    }

    @Override
    public boolean setBusy() {
        return busy.compareAndSet(false, true);
    }

    @Override
    public boolean setUnBusy() {
        return busy.compareAndSet(true, false);
    }
}
