package forks;

/**
 * fork with flag that busy if some philosopher eating this fork
 */
public interface ForkWithBusyFlag extends Fork{
    /**
     * @return true if someone eating this fork
     */
    boolean isBusy();

    /**
     * took this fork
     * @return true if success, false if it already busy or bad lack
     */
    boolean setBusy();

    /**
     * free this fork
     * @return true if success
     */
    boolean setUnBusy();
}
