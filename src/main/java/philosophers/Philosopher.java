package philosophers;

import forks.Fork;

import java.util.concurrent.ThreadLocalRandom;

public abstract class Philosopher implements Runnable{
    public final int position;
    protected Fork left;
    protected Fork right;
    private int eatCount = 0;//count of philosopher launches
    private long waitTime = 0;//time philosopher is hungry
    private long startWait;
    ThreadLocalRandom rnd = ThreadLocalRandom.current();

    public Philosopher(int position){
        this.position = position;
    }

    public  Philosopher(int position, Fork left, Fork right){
        this.position = position;
        this.left = left;
        this.right = right;
    }

    public void eat(){
        waitTime += System.currentTimeMillis() - startWait;
        System.out.println("Philosopher [" + position + "] is eating");

        try {
            Thread.sleep(rnd.nextInt(100)); //eating right now
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        eatCount++;
        System.out.println("Philosopher [" + position + "] is finished eating");
    }

    public void think(){
        System.out.println("Philosopher [" + position + "] is thinking");

        try {
            Thread.sleep(rnd.nextInt(100)); //thinking right now
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Philosopher [" + position + "] is hungry");
        startWait = System.currentTimeMillis();
    }

    public int getEatCount() {
        return eatCount;
    }

    public long getWaitTime() {
        return waitTime;
    }


    protected volatile boolean stopFlag = false;

    protected void runWithSynchronized(){
        while (!stopFlag) {
            think();
            synchronized (left) {
                System.out.println("Philosopher [" + position + "] took left fork");
                synchronized (right) {       //deadlock could be here
                    System.out.println("Philosopher [" + position + "] took right fork");
                    eat();
                }
            }
        }
        System.out.println("Philosopher [" + position + "] stopped");
    }

    public boolean isStopFlag() {
        return stopFlag;
    }

    public void setStopFlag(boolean stopFlag) {
        this.stopFlag = stopFlag;
    }
}
