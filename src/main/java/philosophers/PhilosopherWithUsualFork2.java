package philosophers;

import forks.Fork;

import java.util.concurrent.locks.ReentrantLock;

/**
 *  Bad class. Catch livelock here
 */
public class PhilosopherWithUsualFork2 extends Philosopher{
    public PhilosopherWithUsualFork2(int position, Fork left, Fork right) {
        super(position, left, right);
    }

    ReentrantLock lock = new ReentrantLock();

    public void run() {
        while (!stopFlag) {
            think();

            try{
                lock.tryLock();

            } finally {
                lock.unlock();
            }

            synchronized (left) {
                System.out.println("Philosopher [" + position + "] took left fork");
                synchronized (right) {       //deadlock could be here
                    System.out.println("Philosopher [" + position + "] took right fork");
                    eat();
                }
            }
        }
        System.out.println("Philosopher [" + position + "] stopped");
    }
}
