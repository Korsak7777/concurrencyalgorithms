package philosophers;

import forks.ForkWithBusyFlag;

public final class PhilosopherWithForkWithBusyFlag extends Philosopher {
    private boolean isHungry = false;

    private final ForkWithBusyFlag left;
    private final ForkWithBusyFlag right;
    public PhilosopherWithForkWithBusyFlag(int position, ForkWithBusyFlag left, ForkWithBusyFlag right) {
        super(position);
        this.left = left;
        this.right = right;
    }

    @Override
    public void run() {
        boolean leftSuccess = false;
        boolean rightSuccess = false;

        while (!stopFlag) {
            if(isHungry)
                System.out.println("Philosopher can't think when hungry");
            else
                think();

            class FinalBlock{
                private void _(ForkWithBusyFlag f, boolean success){
                    if(success)
                        f.setUnBusy();
                    else {
                        isHungry = true;
                    }
                }
            }

            try {
                if(leftSuccess = left.setBusy()) {
                    System.out.println("Philosopher [" + position + "] took left fork");
                    try {
                        if(rightSuccess = right.setBusy()) {//no deadlock because if we can't took both forks we release them
                            System.out.println("Philosopher [" + position + "] took right fork");
                            eat();
                            isHungry = false;
                        }
                    } finally {
                        new FinalBlock()._(right, rightSuccess);
                    }
                }
            } finally {
                new FinalBlock()._(left, leftSuccess);
            }
        }
        System.out.println("Philosopher [" + position + "] stopped");
    }
}
