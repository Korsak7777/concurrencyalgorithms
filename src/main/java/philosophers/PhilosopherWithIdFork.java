package philosophers;

import forks.ForkWithID;

public final class PhilosopherWithIdFork extends Philosopher {
    public PhilosopherWithIdFork(int position, ForkWithID fork, ForkWithID anotherFork) {
        super(position);
        //we suppose that one of the fork has odd id
        if(fork.id % 2 == 1){
            left = fork;    //we always blocked odd fork first (see run())
            right = anotherFork;
        } else {
            left = anotherFork;
            right = fork;
        }
    }

    @Override
    public void run() {
        runWithSynchronized();
    }
}
