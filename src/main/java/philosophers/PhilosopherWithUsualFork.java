package philosophers;

import forks.Fork;

/**
 * Bad class. Catch deadlock here
 */
public final class PhilosopherWithUsualFork extends Philosopher {

    public PhilosopherWithUsualFork(int position, Fork left, Fork right) {
        super(position, left, right);
    }

    public void run() {
        runWithSynchronized();
    }
}
