package runners;

import forks.UsualFork;
import philosophers.PhilosopherWithUsualFork;

/**
 * Sometimes not working. Have deadlock problems.
 */
final public class RunPhilosopherWithUsualFork {
    static int count = RunConst.count;
    public static void main(String[] args) throws Exception{
        PhilosopherWithUsualFork[] phils = new PhilosopherWithUsualFork[count];

        UsualFork last = new UsualFork();
        UsualFork left = last;

        for(int i = 0; i < count; i++){
            UsualFork right = (i == count-1) ? last: new UsualFork();
            phils[i] = new PhilosopherWithUsualFork(i, left, right);
            left = right;
        }

        Thread[] threads = new Thread[count]; //TODO change on executors
        for(int i = 0; i < count; i++){
            threads[i] = new Thread(phils[i]);
            threads[i].start();
        }
        Thread.sleep(RunConst.mainThreadSleepingTime);

        for(PhilosopherWithUsualFork phil : phils){
            phil.setStopFlag(true);
        }
        for(Thread thread: threads){
            thread.join();
        }
        for(PhilosopherWithUsualFork phil : phils) {
            System.out.println("[Philosopher " + phil.position + "[ ate " +
                    phil.getEatCount() + " times and waited " + phil.getWaitTime() + " ms");
        }
    }

}
