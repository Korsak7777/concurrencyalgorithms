package runners;

final class RunConst {

    private RunConst(){}

    final static int count = 5;
    final static int mainThreadSleepingTime = 60000;
}
