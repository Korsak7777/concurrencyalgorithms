package runners;

import forks.ForkWithBusyAtomicFlag;
import forks.ForkWithBusyFlag;
import philosophers.PhilosopherWithForkWithBusyFlag;

/**
 * like in @{@link RunPhilosopherWithUsualFork}, but with PhilosopherWithForkWithBusyFlag
 */
final public class RunPhilosopherWithForkWithBusyFlag {

    static int count = RunConst.count;
    public static void main(String[] args) throws Exception{
        PhilosopherWithForkWithBusyFlag[] phils = new PhilosopherWithForkWithBusyFlag[count];

        ForkWithBusyFlag last = new ForkWithBusyAtomicFlag();
        ForkWithBusyFlag left = last;

        for(int i = 0; i < count; i++){
            ForkWithBusyFlag right = (i == count-1) ? last: new ForkWithBusyAtomicFlag();
            phils[i] = new PhilosopherWithForkWithBusyFlag(i, left, right);
            left = right;
        }

        Thread[] threads = new Thread[count]; //TODO change on executors
        for(int i = 0; i < count; i++){
            threads[i] = new Thread(phils[i]);
            threads[i].start();
        }
        Thread.sleep(RunConst.mainThreadSleepingTime);

        for(PhilosopherWithForkWithBusyFlag phil : phils){
            phil.setStopFlag(true);
        }
        for(Thread thread: threads){
            thread.join();
        }
        for(PhilosopherWithForkWithBusyFlag phil : phils) {
            System.out.println("Philosopher [" + phil.position + "] ate " +
                    phil.getEatCount() + " times and waited " + phil.getWaitTime() + " ms");
        }
    }
}
