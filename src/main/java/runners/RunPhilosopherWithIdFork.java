package runners;

import forks.ForkWithID;
import philosophers.PhilosopherWithIdFork;

final public class RunPhilosopherWithIdFork {
    static int count = RunConst.count;
    public static void main(String[] args) throws Exception{
        PhilosopherWithIdFork[] phils = new PhilosopherWithIdFork[count];

        ForkWithID last = new ForkWithID();
        ForkWithID left = last;

        for(int i = 0; i < count; i++){
            ForkWithID right = (i == count-1) ? last: new ForkWithID();
            phils[i] = new PhilosopherWithIdFork(i, left, right);
            left = right;
        }

        Thread[] threads = new Thread[count]; //TODO change on executors
        for(int i = 0; i < count; i++){
            threads[i] = new Thread(phils[i]);
            threads[i].start();
        }
        Thread.sleep(RunConst.mainThreadSleepingTime);

        for(PhilosopherWithIdFork phil : phils){
            phil.setStopFlag(true);
        }
        for(Thread thread: threads){
            thread.join();
        }
        for(PhilosopherWithIdFork phil : phils) {
            System.out.println("Philosopher [" + phil.position + "] ate " +
                    phil.getEatCount() + " times and waited " + phil.getWaitTime() + " ms");
        }
    }
}
